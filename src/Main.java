import com.sun.security.jgss.GSSUtil;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Main{

    static void introduction(){

        WelcomeText getWelcome = new WelcomeText();
        Group3Text text = new Group3Text();
        UnderLineText classRoom= new UnderLineText(text.myClass(),3);
        UnderLineText myGroup3= new UnderLineText(text.myGroup3(),2);
        UnderLineText pleaseWait = new UnderLineText(text.pleaseWait(),2);
        UnderLineText loading = new UnderLineText(text.loadDing(),300);
        getWelcome.start();
        try{
            getWelcome.join();
        }catch(InterruptedException ie){}

        classRoom.start();
        try{
            classRoom.join();
        }catch(InterruptedException ie){}
        myGroup3.start();
        try{
            myGroup3.join();
        }catch(InterruptedException ie){}
        pleaseWait.start();
        try{
            pleaseWait.join();
        }catch(InterruptedException ie){}
        loading.start();
        try{
            loading.join();
        }catch(InterruptedException ie){}


    }

    public static void main(String[] args) throws InterruptedException {
        introduction();
        String cmd ;
        Scanner input = new Scanner(System.in);
        // initialize the product and embedded into the list
        ArrayList<Product> listProduct = new ArrayList<>();

        Product product1 = new Product();
        product1.setName("Coca cola");
        product1.setUnitPrice(12.1);
        product1.setQty(12);
        product1.setDate("11.12.12");

        Product product2 = new Product();
        product2.setName("Fanta");
        product2.setUnitPrice(12.1);
        product2.setQty(12);
        product2.setDate("11.12.12");

        Product product3 =new Product("Red Bull",12.4,12,"12.12.12");
        listProduct.add(product1);
        listProduct.add(product2);
        listProduct.add(product3);




        // Adding Logo here
        boolean isValidCmd=false;
        boolean isNumber =false;
        Product newProduct;
        do {



// initialize the product and embedded into the list
            CellStyle numberStyle1= new CellStyle(CellStyle.HorizontalAlign.center);
            Table table1= new Table(5,BorderStyle.UNICODE_ROUND_BOX,ShownBorders.ALL);
            // Header

            table1.addCell("   ID   ",numberStyle1);
            table1.addCell("  Product Name   ",numberStyle1);
            table1.addCell("   Unit Price   ",numberStyle1);
            table1.addCell("  Quality  ",numberStyle1);
            table1.addCell("    Date    ",numberStyle1);


            for (int i=0; i<listProduct.size(); i++){

                table1.addCell(listProduct.get(i).getId()+"",numberStyle1);
                table1.addCell(listProduct.get(i).getName()+"",numberStyle1);
                table1.addCell(listProduct.get(i).getUnitPrice()+"",numberStyle1);
                table1.addCell(listProduct.get(i).getQty()+"",numberStyle1);
                table1.addCell(listProduct.get(i).getDate()+"",numberStyle1);
            }

            System.out.println(table1.render());
            CellStyle footerStyle= new CellStyle(CellStyle.HorizontalAlign.center);
            Table tableFooter = new Table(1,BorderStyle.UNICODE_ROUND_BOX,ShownBorders.ALL);
            tableFooter.addCell("Page 1 of 1                                        Total Record : 3 ",footerStyle);
            System.out.println(tableFooter.render());



// Showing the Menu
            CellStyle numberStyle= new CellStyle(CellStyle.HorizontalAlign.center);
            Table table = new Table(1,BorderStyle.UNICODE_ROUND_BOX , ShownBorders.ALL);

            table.addCell("Menu List",numberStyle);
            table.addCell(" *)Display       W)rite      U)Update   D)elete        F)irst       P)revious      N)ext     L)ast " , numberStyle);
            table.addCell(" S)earch        G)oto      Se)t row   Sa)ve         B)ack up        Re)store       H)elp     E)xit " , numberStyle);
            System.out.println(table.render());


            System.out.print("Command: ");
            cmd=input.nextLine();

            Pattern commandPattern= Pattern.compile(".*[^a-z].*");
            Pattern numberPattern = Pattern.compile(".*[^0-9].*");

            isValidCmd=commandPattern.matcher(cmd).matches();
            if (!isValidCmd){



                switch (cmd.toLowerCase()){

                    case "*":
                        System.out.println("This is display part ");



                        break;

                    case "w" :

                        String proName;
                        double proPrice;
                        int proQty;
                        System.out.println("This is the write option ");

                        newProduct = new Product();

                        System.out.println("=================== Write a new Product ================");
                        System.out.print("\nProduct ID           : "+newProduct.getId());
                        System.out.print("\nProduct's name       : "); proName= input.nextLine();
                        System.out.print("Product's Price      : "); proPrice= input.nextDouble();
                        System.out.print("Product's Quality     :");     proQty= input.nextInt();
                        newProduct.setName(proName);
                        newProduct.setQty(proQty);
                        newProduct.setUnitPrice(proPrice);
                        LocalDate date = LocalDate.now();
                        newProduct.setDate(date+"");



                        // Show data in form table

                        CellStyle cStyle= new CellStyle(CellStyle.HorizontalAlign.center);
                        Table tb= new Table(2,BorderStyle.UNICODE_ROUND_BOX,ShownBorders.ALL);

                        tb.addCell("        New Product Info    ",2);
                        tb.addCell("    ID    ",cStyle);
                        tb.addCell(newProduct.getId()+"",cStyle);
                        tb.addCell("  Name   ",cStyle);
                        tb.addCell(newProduct.getName(),cStyle);
                        tb.addCell("    Unit Price   ",cStyle);
                        tb.addCell(newProduct.getUnitPrice()+"" ,cStyle);
                        tb.addCell("    Date    ");
                        tb.addCell(newProduct.getDate(),cStyle);


                        System.out.println(tb.render());

                        // need to confirm if user want to add this in or not
                        // Are you sure that you wanna add this record [Y/N]
                        String yesNo;
                        System.out.println(" Are you sure that you want to add it to the list? [Y/y] or [N/n]");
                        yesNo= input.next();input.nextLine();
                        if ( yesNo.toLowerCase().equals("y")){
                            // Add Object to array list
                            listProduct.add(newProduct);
                            System.out.println("=========== Successfully added to the list ==========");

                        }else {

                            System.out.println(" ========== The record has not been saved =======");
                        }

                        onEnterPressed();
                        // save to file ?
                        break;

                    case "u" :
                        int proId=0;
                        boolean isFound=false;
                        //int proIdNum;

                        System.out.println("==================== This is update option ================");
                        System.out.print("Please input ID of the Product: ");
                        // validation
                        try{
                            proId= input.nextInt();

                        }catch (InputMismatchException e){
                            System.out.println(" Can only input the in number format.......");
                        }


                        for (int i=0; i<listProduct.size(); i++){

                            if (proId== listProduct.get(i).getId()){
                                isFound=true;

                                proId  = listProduct.get(i).getId();
                                // Show data in form table
                                CellStyle editStyle= new CellStyle(CellStyle.HorizontalAlign.center);
                                Table editTb= new Table(2,BorderStyle.UNICODE_ROUND_BOX,ShownBorders.ALL);

                                editTb.addCell("        New Product Info    ",2);
                                editTb.addCell("    ID    ",editStyle);
                                editTb.addCell(listProduct.get(i).getId()+"",editStyle);
                                editTb.addCell("  Name   ",editStyle);
                                editTb.addCell(listProduct.get(i).getName(),editStyle);
                                editTb.addCell("    Unit Price   ",editStyle);
                                editTb.addCell(listProduct.get(i).getUnitPrice()+"" ,editStyle);
                                editTb.addCell("    Quantity   ",editStyle);
                                editTb.addCell(listProduct.get(i).getQty()+"" ,editStyle);
                                editTb.addCell("    Date    ");
                                editTb.addCell(listProduct.get(i).getDate(),editStyle);
                                System.out.println(editTb.render());

                            }
                        }// end of the for loop


                        int updateOption=0;

                        if (isFound){
                            do {

                                Table updatedTable = new Table(1,BorderStyle.UNICODE_ROUND_BOX,ShownBorders.ALL);
                                CellStyle updatedStyle= new CellStyle(CellStyle.HorizontalAlign.center);

                                System.out.println("What do you want to update ?");
                                updatedTable.addCell("     1. All   2. Name  3. Quantity  4.Unit Price    5. Back to Menu     ",updatedStyle);
                                System.out.println(updatedTable.render());

                                System.out.print("Option ( 1-5) : ");
                                updateOption=input.nextInt();


                                switch (updateOption){
                                    case 1:
                                        System.out.println("=================== Update the Product ================");
                                        System.out.print("\nProduct ID           : "+listProduct.get(proId-1).getId()); input.nextLine();
                                        System.out.print("\nProduct's name       : "); proName= input.nextLine();
                                        System.out.print("Product's Price      : "); proPrice= input.nextDouble();
                                        System.out.print("Product's Quality     :");     proQty= input.nextInt();


                                        //Should I edit the object as well ?
                                        LocalDate updatedDate= LocalDate.now();
                                        newProduct = (Product) listProduct.get(proId-1);
                                        newProduct.setName(proName);
                                        newProduct.setQty(proQty);
                                        newProduct.setUnitPrice(proPrice);
                                        newProduct.setDate(updatedDate+"");


                                        // show the preview of the update product


                                        CellStyle editStyle= new CellStyle(CellStyle.HorizontalAlign.center);
                                        Table editTb= new Table(2,BorderStyle.UNICODE_ROUND_BOX,ShownBorders.ALL);

                                        editTb.addCell("       Updated Product Info    ",2);
                                        editTb.addCell("    ID    ",editStyle);
                                        editTb.addCell(newProduct.getId()+"",editStyle);
                                        editTb.addCell("  Name   ",editStyle);
                                        editTb.addCell(newProduct.getName(),editStyle);
                                        editTb.addCell("    Unit Price   ",editStyle);
                                        editTb.addCell(newProduct.getUnitPrice()+"" ,editStyle);
                                        editTb.addCell("     Quantity     ",editStyle);
                                        editTb.addCell( newProduct.getQty()+"",editStyle);
                                        editTb.addCell("    Date    ");
                                        editTb.addCell(newProduct.getDate(),editStyle);
                                        System.out.println(editTb.render());


                                        // ask for confirmation
                                        yesNo="";
                                        System.out.println("Are you sure that you want to update this record ? [Y/y]or[N/n]");
                                        yesNo=input.next();
                                        if (yesNo.toLowerCase().equals("y")){
                                            // update the data to the list

                                            listProduct.set(proId-1,newProduct);
                                            System.out.println("Update Successfully !.......");

                                                      /*  listProduct.get(proId-1).setName(proName);
                                                        listProduct.get(proId-1).setDate(updatedDate+"");
                                                        listProduct.get(proId-1).setUnitPrice(proPrice);
                                                        listProduct.get(proId-1).setQty(proQty);
                                                        */

                                        }else {
                                            System.out.println("Update Operation has been canceled .......");

                                        }
                                        onEnterPressed();
                                        break;
                                    case 2:
                                        // Name :
                                        System.out.println("=================== Update the Product ================");input.nextLine();
                                        System.out.print("Product's name       : "); proName= input.nextLine();
                                        newProduct = listProduct.get(proId-1);
                                        newProduct.setName(proName);
                                        // Show data inside the table

                                        CellStyle editStyle1= new CellStyle(CellStyle.HorizontalAlign.center);
                                        Table editTb1= new Table(2,BorderStyle.UNICODE_ROUND_BOX,ShownBorders.ALL);

                                        editTb1.addCell("       Updated Product Info    ",2);
                                        editTb1.addCell("    ID    ",editStyle1);
                                        editTb1.addCell(newProduct.getId()+"",editStyle1);
                                        editTb1.addCell("  Name   ",editStyle1);
                                        editTb1.addCell(newProduct.getName(),editStyle1);
                                        editTb1.addCell("    Unit Price   ",editStyle1);
                                        editTb1.addCell(newProduct.getUnitPrice()+"" ,editStyle1);
                                        editTb1.addCell("    Quantity     ");
                                        editTb1.addCell(newProduct.getQty()+"",editStyle1);
                                        editTb1.addCell("    Date    ");
                                        editTb1.addCell(newProduct.getDate(),editStyle1);
                                        System.out.println(editTb1.render());

                                        // ask for confirmation
                                        yesNo="";
                                        System.out.println("Are you sure that you want to update this record ? [Y/y]or[N/n]");
                                        yesNo=input.next();
                                        if (yesNo.toLowerCase().equals("y")){
                                            // update the data to the list
                                            listProduct.set(proId-1,newProduct);
                                            System.out.println("Update Successfully !.......");

                                        }else {
                                            System.out.println("Update Operation has been canceled .......");
                                        }
                                        onEnterPressed();
                                        break;
                                    case 3:
                                        // Quantity
                                        System.out.println("=================== Update the Product ================");input.nextLine();
                                        System.out.print("Product's Quantity       : "); proQty= input.nextInt();
                                        newProduct = listProduct.get(proId-1);
                                        newProduct.setQty(proQty);

                                        // Show data inside the table
                                        CellStyle editStyle2= new CellStyle(CellStyle.HorizontalAlign.center);
                                        Table editTb2= new Table(2,BorderStyle.UNICODE_ROUND_BOX,ShownBorders.ALL);

                                        editTb2.addCell("       Updated Product Info    ",2);
                                        editTb2.addCell("    ID    ",editStyle2);
                                        editTb2.addCell(newProduct.getId()+"",editStyle2);
                                        editTb2.addCell("  Name   ",editStyle2);
                                        editTb2.addCell(newProduct.getName(),editStyle2);
                                        editTb2.addCell("    Unit Price   ",editStyle2);

                                        editTb2.addCell(newProduct.getUnitPrice()+"" ,editStyle2);
                                        editTb2.addCell("      Quantity    ",editStyle2);
                                        editTb2.addCell(newProduct.getQty()+"",editStyle2);
                                        editTb2.addCell("    Date    ");
                                        editTb2.addCell(newProduct.getDate(),editStyle2);
                                        System.out.println(editTb2.render());

                                        // ask for confirmation
                                        yesNo="";
                                        System.out.println("Are you sure that you want to update this record ? [Y/y]or[N/n]");
                                        yesNo=input.next();
                                        if (yesNo.toLowerCase().equals("y")){
                                            // update the data to the list
                                            listProduct.set(proId-1,newProduct);
                                            System.out.println("Update Successfully !.......");

                                        }else {
                                            System.out.println("Update Operation has been canceled .......");
                                        }
                                        onEnterPressed();


                                        break;
                                    case 4:

                                        // Unit Price
                                        System.out.println("=================== Update the Product ================");input.nextLine();
                                        System.out.print("Product's Unit Price    : "); proPrice = input.nextDouble();
                                        newProduct = listProduct.get(proId-1);
                                        newProduct.setUnitPrice(proPrice);

                                        // Show data inside the table
                                        CellStyle editStyle3= new CellStyle(CellStyle.HorizontalAlign.center);
                                        Table editTb3= new Table(2,BorderStyle.UNICODE_ROUND_BOX,ShownBorders.ALL);

                                        editTb3.addCell("       Updated Product Info    ",2);
                                        editTb3.addCell("    ID    ",editStyle3);
                                        editTb3.addCell(newProduct.getId()+"",editStyle3);
                                        editTb3.addCell("  Name   ",editStyle3);
                                        editTb3.addCell(newProduct.getName(),editStyle3);
                                        editTb3.addCell("    Unit Price   ",editStyle3);

                                        editTb3.addCell(newProduct.getUnitPrice()+"" ,editStyle3);
                                        editTb3.addCell("      Quantity    ",editStyle3);
                                        editTb3.addCell(newProduct.getQty()+"",editStyle3);
                                        editTb3.addCell("    Date    ");
                                        editTb3.addCell(newProduct.getDate(),editStyle3);
                                        System.out.println(editTb3.render());

                                        // ask for confirmation
                                        yesNo="";
                                        System.out.println("Are you sure that you want to update this record ? [Y/y]or[N/n]");
                                        yesNo=input.next();
                                        if (yesNo.toLowerCase().equals("y")){
                                            // update the data to the list
                                            listProduct.set(proId-1,newProduct);
                                            System.out.println("Update Successfully !.......");

                                        }else {
                                            System.out.println("Update Operation has been canceled .......");
                                        }
                                        onEnterPressed();




                                        break;


                                }



                            }while (updateOption!=5);
                        }else {

                            System.out.println( "Record is not found ....................");
                        }
                        onEnterPressed();
                        input.nextLine();

                        break;
                    case "d" :
                        System.out.println("This is delete option ");

                        break;
                    case "f":
                        System.out.println("This is first option ");


                        break;
                    case "p":
                        System.out.println("This is Previous Option");


                        break;
                    case "n":
                        System.out.println("This is next option");
                        break;
                    case "l":
                        System.out.println("This is last option");
                        break;
                    case "s":
                        System.out.println("This is search option"); break;
                    case "g":
                        System.out.println("This is goto option "); break;
                    case "se":
                        System.out.println("This is set row option "); break;
                    case "b":
                        System.out.println("This is back up option"); break;
                    case "sa":
                        System.out.println("Save option"); break;
                    case "re":
                        System.out.println("Restore Option"); break;
                    case "h":
                        System.out.println("Help Option"); break;
                }






            }else {

                System.out.println("Your input is invalid ........");
                System.out.println("Please check the menu and the shortcut........");
                onEnterPressed();
            }






        }while (!( cmd.equals("e") || cmd.equals("E")) );








    }

    private static  void onEnterPressed(){

        System.out.println("Press \"ENTER\" to continue............");
        Scanner scanner= new Scanner(System.in);
        scanner.nextLine();

    }




}

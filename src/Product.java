import java.util.Date;
public class Product {

  private   int id;
  private   String name;
  private    double  unitPrice ;
  private    int qty;

  private    String date;

  private  static int autoNumber=0;

    public Product(String name, double unitPrice, int qty, String date) {
        this.name = name;
        this.unitPrice = unitPrice;
        this.qty = qty;
        this.date = date;
        id=++autoNumber;
    }

    public Product(){

      id=++autoNumber;
  }


    public int getId() {
        return  id;
    }

//    public void setId(int id) {
//        this.id = id;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", unitPrice=" + unitPrice +
                ", qty=" + qty +
                ", date='" + date + '\'' +
                '}';
    }
}

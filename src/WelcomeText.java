public class WelcomeText extends  Thread {
    String text="             Welcome to \n          Stock Management";
    public void run(){
        try {
            char[] getlength = new char[text.length()];
            for (int i = 0; i < text.length(); i++) {
                getlength[i] = text.charAt(i);
            }
            for (char t : getlength) {
                System.out.print(t);
                WelcomeText.sleep(10);
            }
            System.out.println("");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}

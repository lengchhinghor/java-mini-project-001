package com.company.keo_kay;

public class Group3Text {
    public String myClass(){
        String myBTB = "" +
                "\n" +        "        "  +  "\n" +
                "    ██████╗" +  " ████████╗"+"  ██████╗\n" +
                "    ██╔══██╗" + "   ██║"+"     ██╔══██╗\n" +
                "    ██████╦╝" + "   ██║"+"     ██████╦╝\n" +
                "    ██╔══██╗" + "   ██║"+"     ██╔══██╗\n" +
                "    ██████╦╝" +  "   ██║"+"     ██████╦╝\n" +
                "    ╚═════╝"+    "    ╚═╝"+"     ╚═════╝"+"\n";
        return myBTB;
    }
    public String myGroup3(){
        String g3="" +
                "         ██████╗" +"██████╗ \n"+
                "        ██╔════╝" +" ╚════██╗\n"+
                "        ██║  ██╗" +" █████╔╝\n"+
                "        ██║  ╚██╗" +" ╚═══██╗\n"+
                "        ╚██████╔╝" +"██████╔╝\n"+
                "        ╚═════╝"+"╚═════╝ ";
        return g3;
    }
    public String pleaseWait(){
        String wait="\nPlease wait, Loading";
        return wait;
    }
    public String loadDing(){
        String wait=".................\n";
        return wait;
    }
}

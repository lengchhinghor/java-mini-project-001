package com.company.keo_kay;

public class UnderLineText extends Thread{
    String underline;
    int milSecond;
    UnderLineText(String text,int milSecond){
        this.underline=text;
        this.milSecond=milSecond;
    }
    public void run(){
        try {
            char[] getlength = new char[underline.length()];
            for (int i = 0; i < underline.length(); i++) {
                getlength[i] = underline.charAt(i);
            }
            for (char t : getlength) {
                System.out.print(t);
                WelcomeText.sleep(milSecond);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
